import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
// import DemoForm from '@/components/vue/DemoForm'
// import AntdDemo from '@/components/antdvue/AntdDemo'
// import AntdvueDemo from '@/components/antdvue/AntdvueDemo'
// import Editablecell from '@/components/editablecell/Editablecell'
// import TabHome from '@/components/vue/TabHome'
// import Demo from '@/components/Demo'
import Login from '@/components/Login'
import Home from '@/components/Home'
import AppIndex from '@/components/employeeDemo/AppIndex'
import PhotoIndex from '@/components/employeeDemo/PhotoIndex'
import EmployeeList from '@/components/employeeDemo/EmployeeList'
Vue.use(Router)
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: Home,
      // home页面并不需要被访问
      redirect: '/index',
      children: [
        {
          path: '/index',
          name: 'AppIndex',
          component: AppIndex,
          meta: {
            requireAuth: true
          }
        },
        {
          path: '/list',
          name: 'ListIndex',
          component: EmployeeList,
          meta: {
            requireAuth: true
          }
        },
        {
          path: '/photo',
          name: 'PhotoIndex',
          component: PhotoIndex,
          meta: {
            requireAuth: true
          }
        }
      ]
    },
    {
      path: '/',
      name: 'index',
      redirect: '/index',
      component: AppIndex,
      meta: {
        requireAuth: true
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
    // {
    //   path: '/hello',
    //   name: 'HelloWorld',
    //   component: HelloWorld
    // },
    // {
    //   path: '/demoform',
    //   name: 'DemoForm',
    //   component: DemoForm
    // },
    // {
    //   path: '/antd',
    //   name: 'AntdDemo',
    //   component: AntdDemo
    // },
    // {
    //   path: '/antdvue',
    //   name: 'AntdvueDemo',
    //   component: AntdvueDemo
    // },
    // {
    //   path: '/index',
    //   name: 'Demo',
    //   component: Demo,
    //   meta: {
    //     requireAuth: true
    //   },
    //   children: [{
    //     path: '/index',
    //     component: TabHome,
    //     name: 'TabHome',
    //     meta: {
    //       requireAuth: true
    //     }
    //   },
    //   {
    //     path: '/index/vuedemoform',
    //     component: DemoForm,
    //     name: 'DemoForm',
    //     meta: {
    //       requireAuth: true
    //     }
    //   },
    //   {
    //     path: '/index/editablecell',
    //     component: Editablecell,
    //     name: 'Editablecell',
    //     meta: {
    //       requireAuth: true
    //     }
    //   },
    //   {
    //     path: '/index/antdvuedemo',
    //     name: 'AntdvueDemo',
    //     component: AntdvueDemo,
    //     meta: {
    //       requireAuth: true
    //     }
    //   }
    //   ]
    // }
  ]
})
