# vue_demo

>  A vue.js demo

## Preparation

- [下载](https://nodejs.org/zh-cn/) node.js (版本8.9以上)，下载完在命令行中查看版本：
  ```
  node -v
  ```
- 安装 cnpm:
  ```
  npm install -g cnpm --registry=https://registry.npm.taobao.org
  ```
- 全局安装 vue-cli：
  ```
  cnpm install vue-cli -g
  ```

## 运行 vue 项目
```
cd vue_demo
npm run dev
```

#### 运行[后台项目](https://gitlab.com/GuoAdeline/spring-vue-demo)
#### 查看页面
- 访问地址：localhost:8080 (admin/123)

#### 页面示例
- 登录页面：

  ![](./figs/login.png)
  
- 首页：

  ![](./figs/shouye.jpg)
  
- 列表：

  ![](./figs/list.jpg)
  
- 注册表单：

  ![](./figs/form.jpg)
  
- 照片展示：

  ![](./figs/photo.jpg)
  
## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
