# VUE demo 
> GUO Mengxue

> 2020.9.3

## vue安装

- [下载](https://nodejs.org/zh-cn/) node.js (版本8.9以上)，下载完在命令行中查看版本：
  ```
  node -v
  ```
- 安装 cnpm:
  ```
  npm install -g cnpm --registry=https://registry.npm.taobao.org
  ```
  查看版本：
  ```
  npm -v
  ```
- 全局安装 vue-cli：
  ```
  cnpm install vue-cli -g
  ```
  查看版本：
  ```
  vue -V
  ```
  
## 创建 vue 项目
```
vue init webpack vue_demo    # 这里需要进行一些配置，默认回车即可
```

## 运行 vue 项目
```
cd vue_demo
npm run dev
```

### VUE表单页面访问地址：localhost:8080/demoform
### ANTD VUE表单访问地址：localhost:8080/antdvue
### 整合页面地址：localhost:8080

---

> **Tips 1: IDEA中运行vue项目**

> - 配置vue插件
  
>  点击File-->Settings-->Plugins-->搜索vue.js插件进行安装
  
> - 运行项目
  
>  点击edit configurations配置
  
>  ![](./figs/vue_conf1.png)
   
>  添加一个npm：
  
>  ![](./figs/vue_conf2.png)
---

> **Tips 2: 在项目中插入favicon图标**

> 1、开发环境（开发调试时）配置：
  
>  ![](./figs/favicon_dev1.png)
  
>  ![](./figs/favicon_dev2.png)
  
> 2、生产环境（打包后）配置：

>  ![](./figs/favicon_prod1.png)
  
>  ![](./figs/favicon_prod2.png)
---

## 引入antd vue
- 安装
  
  ```
  npm install ant-design-vue --save
  ```
  然后在`main.js`里面引入：
  ```
  import Antd from 'ant-design-vue'
  import 'ant-design-vue/dist/antd.css'
  
  Vue.use(Antd)
  ```

## 连接数据库

数据库地址：192.168.56.66:3306 (root/123456)

navicat连不上数据库的[解决方法](https://blog.csdn.net/weixin_43837883/article/details/88573367)

## 引入Vuex
Vuex 是专门为 Vue 开发的状态管理方案，可以把需要在各个组件中传递使用的变量、方法定义在这里

```
npm install vuex --save
```
